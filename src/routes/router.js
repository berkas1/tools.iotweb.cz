/**
 * Created by berkas1 on 10.9.16.
 */

var express = require('express');
var router = express.Router();





router.get('/ip', function(req, res, next) {
   res.send(req.ip);
});

router.get('/ip/json', function(req, res, next) {
    res.json({"ip" : req.ip});
});

module.exports = router;