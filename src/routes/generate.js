/**
 * Created by berkas1 on 10.9.16.
 */

var express = require('express');
var router = express.Router();
var random = require("random-js")();
var crypto = require('crypto');


function getTime() {
    var time = (new Date().getTime()).toString();
    time = (time.slice(0, -3))*1;

    return time;
}


function getRandomString() {
    var time = getTime();

    var randomNumber = random.integer(1, 9999999) + time + random.integer(1, 9999999);
    var hash = crypto.createHash("sha256");
    hash.update(randomNumber.toString(), "utf8");

    return hash.digest("hex");
}


router.get('/random', function(req, res) {
    res.status(200);
    res.send(getRandomString().toString());

});

router.get('/random/json', function(req, res) {
    res.status(200);
    res.json({"string" : getRandomString().toString()});

});

module.exports = router;