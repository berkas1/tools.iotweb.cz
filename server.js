/**
 * Created by berkas1 on 10.9.16.
 */

var express = require('express');
var path = require('path');

var app = express();
app.enable('trust proxy');


var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

// define routes
var router = require('./src/routes/router.js');
var generate = require('./src/routes/generate.js');


app.listen(server_port, server_ip_address, null, function() {
    console.log('Example app listening on port 3000!!');
});


app.get('/', function(req, res, next) {
    //res.send('Default page for tools.iotweb.cz.');
    res.sendFile(path.resolve("src/html/main.html"));
});

app.get('/*',function(req,res,next){
    res.header('X-Powered-By', 'tools.iotweb.cz');
    res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
    res.header('Pragma', 'no-cache');

    next();
});


app.use('/', router);
app.use('/generate', generate);
